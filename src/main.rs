use std::{
    path::{Path, PathBuf},
    process,
    sync::Arc,
};

use structopt::StructOpt;

#[tokio::main]
async fn main() {
    let opt = Opt::from_args();

    let remove_options = Arc::new(RemoveOptions {
        force: opt.force,
        dir: opt.dir,
        recursive: opt.recursive,
    });

    let mut tasks = Vec::with_capacity(opt.files.len());

    for file in opt.files {
        let remove_options = Arc::clone(&remove_options);
        tasks.push(tokio::spawn(async move {
            if let Err(err) = remove(&file, &remove_options).await {
                eprintln!("Cannot remove '{}': {}", file.display(), err);
                Err(())
            } else {
                Ok(())
            }
        }));
    }

    let mut any_failed = false;

    for task in tasks {
        if task.await.unwrap().is_err() {
            any_failed = true;
        }
    }

    if any_failed {
        process::exit(1);
    }
}

#[derive(StructOpt)]
struct Opt {
    #[structopt(parse(from_os_str))]
    files: Vec<PathBuf>,

    #[structopt(short, long)]
    force: bool,

    #[structopt(short, long)]
    dir: bool,

    #[structopt(short, long)]
    recursive: bool,
}

struct RemoveOptions {
    force: bool,
    dir: bool,
    recursive: bool,
}

async fn remove(path: impl AsRef<Path>, opts: &RemoveOptions) -> Result<(), RemoveError> {
    let path = path.as_ref();
    let is_dir = path.is_dir();

    if !opts.force && !path.exists() {
        Err(RemoveError::DoesNotExist)
    } else if !opts.dir && !opts.recursive && is_dir {
        Err(RemoveError::IsDir)
    } else if !opts.recursive
        && is_dir
        && tokio::fs::read_dir(path)
            .await?
            .next_entry()
            .await?
            .is_some()
    {
        Err(RemoveError::DirNotEmpty)
    } else {
        trash::delete(path).map_err(RemoveError::Trash)
    }
}

#[derive(Debug, thiserror::Error)]
enum RemoveError {
    #[error(transparent)]
    Trash(trash::Error),

    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error("No such file or directory.")]
    DoesNotExist,

    #[error("Is a directory.")]
    IsDir,

    #[error("Directory not empty.")]
    DirNotEmpty,
}

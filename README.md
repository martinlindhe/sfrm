# sfrm

![Crates.io](https://img.shields.io/crates/v/sfrm)
![Crates.io](https://img.shields.io/crates/d/sfrm)
![Crates.io](https://img.shields.io/crates/l/sfrm)

> `alias rm="sfrm"` and never remove anything accidentally.

Simple alternative to `rm` command, which puts removed files to trash instead of
removing them. I think it doesn't need much explanation of why is it useful.

CLI usage should be 100% compatible with `rm` command. But it isn't as I don't
really need that. So instead it currently supports only the things listed in the
table below and I'll implement more of them when I'll need them or someone will
request it.

| Feature                   | Supported |
| ------------------------- | --------- |
| `-f`, `--force`           | Yes       |
| `-i`                      | No        |
| `-I`                      | No        |
| `--interactive[=WHEN]`    | No        |
| `--one-file-system`       | No        |
| `--preserve-root[=all]`   | No        |
| `-r`, `-R`, `--recursive` | Yes\*     |
| `-d`, `--dir`             | Yes       |
| `-v`, `--verbose`         | No        |
| `--help`                  | Yes       |
| `--version`               | Yes       |
| Compatible output         | No        |

\* Only `-r` and `--recursive` are supported.

## Installation

```sh
cargo install sfrm
echo 'alias rm="sfrm"' >> ~/.bashrc # (optional) replace .bashrc with appropriate file for your shell
```

## License

This project is licensed under the MIT license or the Apache License (Version
2.0).

See [LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT) for details.
